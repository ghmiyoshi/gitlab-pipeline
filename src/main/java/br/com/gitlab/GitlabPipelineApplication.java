package br.com.gitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabPipelineApplication.class, args);
	}

}
